package dwfe.modules.mailing;

import dwfe.config.DwfeConfigProperties;
import dwfe.modules.mailing.config.MailingConfigProperties;
import dwfe.modules.mailing.db.mailing.MailingPersonal;
import dwfe.modules.mailing.db.mailing.MailingPersonalService;
import dwfe.test.DwfeTestAuth;
import dwfe.test.DwfeTestUtil;
import dwfe.test.config.DwfeTestConfigProperties;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static dwfe.modules.mailing.MailingTestVariablesForApiTest.checkers_for_personal;
import static dwfe.test.DwfeTestUtil.pleaseWait;
import static org.junit.Assert.*;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

//
// == https://spring.io/guides/gs/testing-web/
//

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT  // == https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html#boot-features-testing-spring-boot-applications
)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MailingTest
{
  private static final Logger log = LoggerFactory.getLogger(MailingTest.class);

  @Autowired
  private DwfeConfigProperties propDwfe;
  @Autowired
  private MailingConfigProperties propMailing;
  @Autowired
  private DwfeTestConfigProperties propDwfeTest;
  @Autowired
  private DwfeTestUtil utilTest;
  @Autowired
  private DwfeTestAuth auth;
  @Autowired
  private MailingPersonalService mailingPersonalService;


  //-------------------------------------------------------
  // ACTUATOR
  //

  @Test
  public void _00_01_actuator()
  {
    pleaseWait(65, log);
    utilTest.test_actuator();
  }


  //-------------------------------------------------------
  // Personal
  //

  @Test
  public void _01_01_personal() throws InterruptedException
  {
    var timeToWait = TimeUnit.MILLISECONDS.toSeconds(propMailing.getScheduledTaskMailing().getCollectFromDbInterval()) * 4;
    var module = "NEVIS";
    var type0 = "WELCOME_ONLY";
    var type1 = "WELCOME_PASSWORD";
    var type2 = "PASSWORD_WAS_CHANGED";
    var type3 = "PASSWORD_RESET_CONFIRM";
    var type4 = "EMAIL_CONFIRM";
    var email = "test1@dwfe.ru";
    var apiRoot = propDwfe.getApiRoot();

    assertEquals(0, mailingPersonalService.findByModuleAndTypeAndEmail(module, type0, email).size());
    assertEquals(0, mailingPersonalService.findByModuleAndTypeAndEmail(module, type1, email).size());
    assertEquals(0, mailingPersonalService.findByModuleAndTypeAndEmail(module, type2, email).size());
    assertEquals(0, mailingPersonalService.findByModuleAndTypeAndEmail(module, type3, email).size());
    assertEquals(0, mailingPersonalService.findByModuleAndTypeAndEmail(module, type4, email).size());

    var resource = propMailing.getResource().getPersonal();
    utilTest.check(apiRoot + resource, POST, auth.getAnonym_accessToken(), checkers_for_personal);

    pleaseWait(timeToWait, log);
    var letter = getMailingFirstOfOne(module, type0, email); // WELCOME_ONLY
    assertFalse(letter.isMaxAttemptsReached());
    assertTrue(letter.isSent());
    assertNull(letter.getData());

    letter = getMailingFirstOfOne(module, type1, email); // WELCOME_PASSWORD
    assertFalse(letter.isMaxAttemptsReached());
    assertTrue(letter.isSent());
    assertNull(letter.getData());

    letter = getMailingFirstOfOne(module, type2, email); // PASSWORD_WAS_CHANGED
    assertFalse(letter.isMaxAttemptsReached());
    assertTrue(letter.isSent());
    assertNull(letter.getData());

    letter = getMailingFirstOfOne(module, type3, email); // PASSWORD_RESET_CONFIRM
    assertFalse(letter.isMaxAttemptsReached());
    assertTrue(letter.isSent());
    assertNull(letter.getData());

    letter = getMailingFirstOfOne(module, type4, email); // EMAIL_CONFIRM
    assertFalse(letter.isMaxAttemptsReached());
    assertTrue(letter.isSent());
    assertNull(letter.getData());
  }


  //-------------------------------------------------------
  // UTILs
  //

  private List<MailingPersonal> getMailingListByModuleAndTypeAndEmail(String module, String type, String email)
  {
    return mailingPersonalService.findByModuleAndTypeAndEmail(module, type, email);
  }

  private MailingPersonal getMailingFirstOfOne(String module, String type, String email)
  {
    var mailing = getMailingListByModuleAndTypeAndEmail(module, type, email);
    assertEquals(1, mailing.size());
    return mailing.get(0);
  }
}
