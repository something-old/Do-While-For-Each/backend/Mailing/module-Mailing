package dwfe.test;

import dwfe.test.config.DwfeTestConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DwfeTestClient
{
  @Autowired
  private DwfeTestConfigProperties propTest;

  public String clientname;
  public String clientpass;
  public int maxTokenExpirationTime;
  public int minTokenExpirationTime;

  public DwfeTestClient of(String clientname, String clientpass, int maxTokenExpirationTime, int minTokenExpirationTime)
  {
    var client = new DwfeTestClient();
    client.clientname = clientname;
    client.clientpass = clientpass;
    client.maxTokenExpirationTime = maxTokenExpirationTime;
    client.minTokenExpirationTime = minTokenExpirationTime;
    return client;
  }

  public DwfeTestClient getClientTrusted()
  {
    return of(
            propTest.getOauth2ClientTrusted().getId(),
            propTest.getOauth2ClientTrusted().getPassword(),
            1_728_000,
            180
    );
  }

  public DwfeTestClient getClientUntrusted()
  {
    return of(
            propTest.getOauth2ClientUntrusted().getId(),
            propTest.getOauth2ClientUntrusted().getPassword(),
            180,
            0
    );
  }
}
