USE dwfe_dev;

SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS
dwfe_mailing_personal;
SET FOREIGN_KEY_CHECKS = 1;

--
-- MODULE: Mailing
--

CREATE TABLE dwfe_mailing_personal (
  created_on            DATETIME                             NOT NULL                DEFAULT CURRENT_TIMESTAMP,
  module                VARCHAR(100)                         NOT NULL,
  `type`                VARCHAR(100)                         NOT NULL,
  email                 VARCHAR(100)                         NOT NULL,
  sent                  TINYINT(1)                           NOT NULL,
  max_attempts_reached  TINYINT(1)                           NOT NULL,
  data                  VARCHAR(2000),
  cause_of_last_failure VARCHAR(2000),
  updated_on            DATETIME ON UPDATE CURRENT_TIMESTAMP NOT NULL                DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (created_on, module, `type`, email)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;