package dwfe.modules.mailing.db.mailing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class MailingPersonalService
{
  private final MailingPersonalRepository repository;

  @Autowired
  public MailingPersonalService(MailingPersonalRepository repository)
  {
    this.repository = repository;
  }

  public List<MailingPersonal> getNewJob()
  {
    return repository.getNewJob();
  }

  public List<MailingPersonal> findByModuleAndEmail(String module, String email)
  {
    return repository.findByModuleAndEmail(module, email);
  }

  public List<MailingPersonal> findByModuleAndTypeAndEmail(String module, String type, String email)
  {
    return repository.findByModuleAndTypeAndEmail(module, type, email);
  }

  public Optional<MailingPersonal> findLastByModuleAndTypeAndEmail(String module, String type, String email)
  {
    return repository.findLastByModuleAndTypeAndEmail(module.toString(), type.toString(), email);
  }

  public Optional<MailingPersonal> findByModuleAndTypeAndData(String module, String type, String data)
  {
    return repository.findByModuleAndTypeAndData(module, type, data);
  }

  public List<MailingPersonal> findSentNotEmptyData(String module, String type, String email)
  {
    return repository.findSentNotEmptyData(module.toString(), type.toString(), email);
  }

  public Optional<MailingPersonal> findLastSentNotEmptyData(String module, String type, String email)
  {
    return repository.findLastSentNotEmptyData(module.toString(), type.toString(), email);
  }

  @Transactional
  public void save(MailingPersonal mailing)
  {
    if (mailing.getEmail() != null)
      repository.save(mailing);
  }

  @Transactional
  public void saveAll(List<MailingPersonal> list)
  {
    repository.saveAll(list);
  }

  @Transactional
  public void deleteAll()
  {
    repository.deleteAll();
  }
}
