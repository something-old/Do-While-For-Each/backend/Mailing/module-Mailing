package dwfe.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
@Configuration
@ConfigurationProperties(prefix = "dwfe")
public class DwfeConfigProperties implements InitializingBean
{
  private final static Logger log = LoggerFactory.getLogger(DwfeConfigProperties.class);

  private String appName;

  private Resource resource = new Resource();

  private DwfeConfigServiceNames service;

  private final Environment env;

  @Autowired
  public DwfeConfigProperties(Environment env)
  {
    this.env = env;
  }

  @Override
  public void afterPropertiesSet() throws Exception
  {
    appName = env.getProperty("spring.application.name");

    if ("NEVIStest".equals(appName))
    {
      var gatewayPath = getService().getNevis().getGatewayPath();
      getService().getNevis().setGatewayPath(gatewayPath.replaceAll("/dev/", "/test/"));
    }

    log.info(toString());
  }

  public static class Resource
  {
    private String actuator = "/actuator";

    public String getActuator()
    {
      return actuator;
    }

    public void setActuator(String actuator)
    {
      this.actuator = actuator;
    }
  }

  public static class DwfeConfigServiceNames
  {
    @NotNull
    private String gatewayUrl;
    @NotNull
    private DwfeConfigServiceNamesSource registry;
    @NotNull
    private DwfeConfigServiceNamesSource config;
    @NotNull
    private DwfeConfigServiceNamesSource gateway;
    @NotNull
    private DwfeConfigServiceNamesSource mailing;
    @NotNull
    private DwfeConfigServiceNamesSource common;
    @NotNull
    private DwfeConfigServiceNamesSource storage;
    @NotNull
    private DwfeConfigServiceNamesSource nevis;
    @NotNull
    private DwfeConfigServiceNamesSource bali;

    public String getGatewayUrl()
    {
      return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl)
    {
      this.gatewayUrl = gatewayUrl;
    }

    public DwfeConfigServiceNamesSource getRegistry()
    {
      return registry;
    }

    public void setRegistry(DwfeConfigServiceNamesSource registry)
    {
      this.registry = registry;
    }

    public DwfeConfigServiceNamesSource getConfig()
    {
      return config;
    }

    public void setConfig(DwfeConfigServiceNamesSource config)
    {
      this.config = config;
    }

    public DwfeConfigServiceNamesSource getGateway()
    {
      return gateway;
    }

    public void setGateway(DwfeConfigServiceNamesSource gateway)
    {
      this.gateway = gateway;
    }

    public DwfeConfigServiceNamesSource getMailing()
    {
      return mailing;
    }

    public void setMailing(DwfeConfigServiceNamesSource mailing)
    {
      this.mailing = mailing;
    }

    public DwfeConfigServiceNamesSource getCommon()
    {
      return common;
    }

    public void setCommon(DwfeConfigServiceNamesSource common)
    {
      this.common = common;
    }

    public DwfeConfigServiceNamesSource getStorage()
    {
      return storage;
    }

    public void setStorage(DwfeConfigServiceNamesSource storage)
    {
      this.storage = storage;
    }

    public DwfeConfigServiceNamesSource getNevis()
    {
      return nevis;
    }

    public void setNevis(DwfeConfigServiceNamesSource nevis)
    {
      this.nevis = nevis;
    }

    public DwfeConfigServiceNamesSource getBali()
    {
      return bali;
    }

    public void setBali(DwfeConfigServiceNamesSource bali)
    {
      this.bali = bali;
    }
  }

  public static class DwfeConfigServiceNamesSource
  {
    String name;
    String gatewayPath;

    public String getName()
    {
      return name;
    }

    public void setName(String name)
    {
      this.name = name;
    }

    public String getGatewayPath()
    {
      return gatewayPath;
    }

    public void setGatewayPath(String gatewayPath)
    {
      this.gatewayPath = gatewayPath;
    }
  }

  public String getApiRoot()
  {
    return "http://" + getAppName();
  }

  public String getAppName()
  {
    return appName;
  }

  public void setAppName(String appName)
  {
    this.appName = appName;
  }

  public Resource getResource()
  {
    return resource;
  }

  public void setResource(Resource resource)
  {
    this.resource = resource;
  }

  public DwfeConfigServiceNames getService()
  {
    return service;
  }

  public void setService(DwfeConfigServiceNames service)
  {
    this.service = service;
  }

  @Override
  public String toString()
  {
    var mainInfo = String.format("" +
                    "|                                                     %n" +
                    "| App Name:              %s%n" +
                    "| API Root:              %s%n" +
                    "|                                                     %n" +
                    "|                                                     %n" +
                    "| Resources                                           %n" +
                    "|      %s%n",
            appName,
            getApiRoot(),

            resource.actuator
    );

    var serviceNames = service == null ? "" : String.format("" +
                    "|                                                     %n" +
                    "|                                                     %n" +
                    "| Service names source                                %n" +
                    "|                                                     %n" +
                    "|   Gateway URL:         %s%n" +
                    "|                                                     %n" +
                    "|   REGISTRY:                                         %n" +
                    "|     name:              %s%n" +
                    "|     gateway path:      %s%n" +
                    "|                                                     %n" +
                    "|   CONFIG:                                           %n" +
                    "|     name:              %s%n" +
                    "|     gateway path:      %s%n" +
                    "|                                                     %n" +
                    "|   GATEWAY:                                          %n" +
                    "|     name:              %s%n" +
                    "|     gateway path:      %s%n" +
                    "|                                                     %n" +
                    "|   MAILING:                                          %n" +
                    "|     name:              %s%n" +
                    "|     gateway path:      %s%n" +
                    "|                                                     %n" +
                    "|   COMMON:                                           %n" +
                    "|     name:              %s%n" +
                    "|     gateway path:      %s%n" +
                    "|                                                     %n" +
                    "|   STORAGE:                                          %n" +
                    "|     name:              %s%n" +
                    "|     gateway path:      %s%n" +
                    "|                                                     %n" +
                    "|   NEVIS:                                            %n" +
                    "|     name:              %s%n" +
                    "|     gateway path:      %s%n" +
                    "|                                                     %n" +
                    "|   BALI:                                             %n" +
                    "|     name:              %s%n" +
                    "|     gateway path:      %s%n",
            service.gatewayUrl,

            service.registry.name,
            service.registry.gatewayPath,

            service.config.name,
            service.config.gatewayPath,

            service.gateway.name,
            service.gateway.gatewayPath,

            service.mailing.name,
            service.mailing.gatewayPath,

            service.common.name,
            service.common.gatewayPath,

            service.storage.name,
            service.storage.gatewayPath,

            service.nevis.name,
            service.nevis.gatewayPath,

            service.bali.name,
            service.bali.gatewayPath
    );

    return String.format("%n%n" +
                    "-====================================================-%n" +
                    "|                 Do|While|For|Each                  |%n" +
                    "|----------------------------------------------------|%n" +
                    "%s" +
                    "%s" +
                    "|_____________________________________________________%n%n",
            mainInfo,
            serviceNames
    );
  }
}
